mod world;

use self::world::world::World;
use self::world::cell::Cell;

fn main() {
    let mut world: World = World {
        size: 2,
        cells: vec![
            Cell { x: 0, y: 0, alive: true },
            Cell { x: 1, y: 0, alive: false },
            Cell { x: 0, y: 1, alive: false },
            Cell { x: 1, y: 1, alive: true },
        ]
    };
    println!("{}", world);

    world = world.evolve();
    println!("{}", world);

    world = world.evolve();
    println!("{}", world);
}
