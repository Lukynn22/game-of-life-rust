use std::cmp::{max, min};
use std::fmt::{self, Formatter, Display};
use super::cell::Cell;

pub struct World {
    pub size: u32,
    pub cells: Vec<Cell>,
}

impl World {
    pub fn evolve(&self) -> World {
        World {
            size: self.size,
            cells: self.cells.iter()
                .map(|cell: &Cell | cell.evolve(World::count_number_of_neighbours(cell, self)))
                .collect()
        }
    }

    fn count_number_of_neighbours(cell: &Cell, world: &World) -> u32 {
        let mut live_neighbors: u32 = 0;

        let max_cell_x = if cell.x == 0 { cell.x } else { cell.x - 1 };
        let max_cell_y = if cell.y == 0 { cell.y } else { cell.y - 1 };

        let min_x = max(0, max_cell_x);
        let max_x = min(world.size - 1, cell.x + 1);
        let min_y = max(0, max_cell_y);
        let max_y = min(world.size - 1, cell.y + 1);

        for x in min_x..=max_x {
            for y in min_y..=max_y {
                if x == cell.x && y == cell.y {
                    continue;
                }

                if world.is_cell_on_position_alive(x, y) == true {
                    live_neighbors += 1;
                }
            }
        }

        live_neighbors
    }

    fn is_cell_on_position_alive(&self, x: u32, y: u32) -> bool {
        let cell = self.cells.iter()
            .find(|cell: &&Cell | cell.x == x && cell.y == y);
        cell.is_some() && cell.unwrap().alive
    }
}

impl Display for World {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "World size: {0}x{0}\nCell state:\n", self.size)?;
        self.cells.iter()
            .for_each(|cell: &Cell| println!("{}", cell));
        write!(f, "")
    }
}
