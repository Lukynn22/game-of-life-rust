use std::fmt::{self, Formatter, Display};

pub struct Cell {
    pub x: u32,
    pub y: u32,
    pub alive: bool,
}

impl Cell {
    pub fn evolve(&self, number_of_neighbours: u32) -> Cell {
        let alive = if self.alive == false && number_of_neighbours == 3 {
            true
        } else if self.alive && (number_of_neighbours == 2 || number_of_neighbours == 3) {
            false
        } else {
            false
        };
        Cell { x: self.x, y: self.y, alive }
    }
}

impl Display for Cell {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(f, "Cell {{ x: {}, y: {}, alive: {} }}", self.x, self.y, self.alive)
    }
}
